/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.punkyideas.timetracker.model;

import java.util.*;
import javax.persistence.*;
import java.io.Serializable;

/**
 *
 * @author jamie
 */
@Entity(name = "CLIENT") //Name of the entity
public class Client implements Serializable {

    @Id
    @GeneratedValue
    private int id;
    private String name;
    private String note;
    private boolean university;
    private double rate;
    private String icon;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @OneToMany(targetEntity = Contact.class, mappedBy = "client", cascade = CascadeType.ALL)
    private List<Contact> clientContact;

    @OneToMany(targetEntity = Project.class, mappedBy = "client", cascade = CascadeType.ALL)
    private List<Project> projects;

    @OneToMany(targetEntity = Task.class, mappedBy = "client", cascade = CascadeType.ALL)
    private List<Task> tasks;

    public Client() {
        this.university = false;
        this.rate = 0.00;
        this.projects = new ArrayList();
        this.tasks = new ArrayList();
        this.clientContact = new ArrayList();
        this.projects.add(new Project("Client Tasks", this));
        this.icon = "/com/punkyideas/timetracker/view/coins.png";

    }

    public Client(String cName, String cRate, String cNotes) {
        this.name = cName;
        this.rate = Double.parseDouble(cRate);
        this.note = cNotes;

        this.university = false;
        this.icon = "/com/punkyideas/timetracker/view/coins.png";
        this.rate = Double.parseDouble(cRate);
        this.projects = new ArrayList();
        this.clientContact = new ArrayList();
        this.tasks = new ArrayList();
        this.projects.add(new Project("Client Tasks", this));
    }

    public Client(String cName, boolean isUni, String cNotes) {
        this.name = cName;
        this.rate = 0.00;
        this.note = cNotes;

        this.university = isUni;
        if (isUni) {
            this.icon = "/com/punkyideas/timetracker/view/lightbulb.png";
        } else {
            this.icon = "/com/punkyideas/timetracker/view/coins.png";
        }
        this.projects = new ArrayList();
        this.clientContact = new ArrayList();
        this.tasks = new ArrayList();
        this.projects.add(new Project("Client Tasks", this));
    }

    public void addProject(Project nProject) {
        nProject.setClient(this);
        this.projects.add(nProject);
    }

    public void addContact(Contact nContact) {
        nContact.setClient(this);
        this.clientContact.add(nContact);
    }

    public void addTask(Task nTask) {
        nTask.setClient(this);
        this.tasks.add(nTask);
    }

    public void addTask(String nName, String nNote, int tPriority, String dDate, String nRate) {
        Task newTask = new Task(nName, nNote, tPriority, dDate, nRate, this);
        this.tasks.add(newTask);
    }

    @Override
    public String toString() {
        return this.name;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the note
     */
    public String getNote() {
        return note;
    }

    /**
     * @param note the note to set
     */
    public void setNote(String note) {
        this.note = note;
    }

    /**
     * @return the university
     */
    public boolean isUniversity() {
        return university;
    }

    /**
     * @param university the university to set
     */
    public void setUniversity(boolean university) {
        this.university = university;
    }

    /**
     * @return the rate
     */
    public double getRate() {
        return rate;
    }

    /**
     * @param rate the rate to set
     */
    public void setRate(double rate) {
        this.rate = rate;
    }

    /**
     * @return the user
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * @return the clientContact
     */
    public List<Contact> getClientContact() {
        return clientContact;
    }

    /**
     * @param clientContact the clientContact to set
     */
    public void setClientContact(List<Contact> clientContact) {
        this.clientContact = clientContact;
    }

    /**
     * @return the projects
     */
    public List<Project> getProjects() {
        return projects;
    }

    /**
     * @param projects the projects to set
     */
    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }

    /**
     * @return the tasks
     */
    public List<Task> getTasks() {
        return tasks;
    }

    /**
     * @param tasks the tasks to set
     */
    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    /**
     * @return the icon
     */
    public String getIcon() {
        return icon;
    }

    /**
     * @param icon the icon to set
     */
    public void setIcon(String icon) {
        this.icon = icon;
    }

}
