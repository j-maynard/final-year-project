
package com.punkyideas.timetracker.model;

import java.io.Serializable;
import java.util.*;
import javax.persistence.*;


@Entity(name = "USER")
public class User implements Serializable {
    
    @Id
    @GeneratedValue
    private int id;
    private String username;
    private String passwordhash;
    private String email;
    private String firstname;
    private String lastname;
    
    @OneToMany(targetEntity=Client.class, mappedBy = "user", cascade=CascadeType.ALL)
    private List<Client> clients;
    
    public User() {
        
    }
    
}
