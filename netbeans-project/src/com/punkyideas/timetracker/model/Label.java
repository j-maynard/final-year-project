/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.punkyideas.timetracker.model;
import java.io.Serializable;
import java.util.*;
import javax.persistence.*;
/**
 *
 * @author jamie
 */

@Entity(name = "LABEL")
public class Label implements Serializable {
    
    @Id
    @GeneratedValue
    private String id;
    
    private String color;
    private String name;
    
}
