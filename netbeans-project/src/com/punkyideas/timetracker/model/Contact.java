/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.punkyideas.timetracker.model;

import java.io.Serializable;
import javax.persistence.*;

@Entity(name = "CONTACT")
public class Contact  implements Serializable {
    
    @Id
    @GeneratedValue
    private int id;
    
    @ManyToOne
    @JoinColumn(name = "client_id")
    private Client client;
    
    private String contactname;
    private String email;
    private String tel;
    private String firstline;
    private String secondline;
    private String towncity;
    private String countystate;
    private String postcode;
    private String country;
    private String notes;
    
    public Contact() {
        this.contactname = "";
        this.email = "";
        this.firstline = "";
        this.secondline = "";
        this.towncity = "";
        this.countystate = "";
        this.postcode = "";
        this.country = "United Kingdom";
        this.notes = "";
    }
    
    public Contact(String cName, String cEmail, String cTel, String cNotes) {
        this.contactname = cName;
        this.email = cEmail;
        this.tel = cTel;
        this.notes = cNotes;
        this.country = "United Kingdom";
    }
    
    public Contact(String cName, String cEmail, String cTel, String cNotes, Client cClient) {
        this.contactname = cName;
        this.email = cEmail;
        this.tel = cTel;
        this.notes = cNotes;
        this.client = cClient;
        this.country = "United Kingdom";
    }
    
    public String toString() {
        return this.contactname;
    }

    /**
     * @return the client
     */
    public Client getClient() {
        return client;
    }

    /**
     * @param client the client to set
     */
    public void setClient(Client client) {
        this.client = client;
    }

    /**
     * @return the contactname
     */
    public String getContactname() {
        return contactname;
    }

    /**
     * @param contactname the contactname to set
     */
    public void setContactname(String contactname) {
        this.contactname = contactname;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the tel
     */
    public String getTel() {
        return tel;
    }

    /**
     * @param tel the tel to set
     */
    public void setTel(String tel) {
        this.tel = tel;
    }

    /**
     * @return the firstline
     */
    public String getFirstline() {
        return firstline;
    }

    /**
     * @param firstline the firstline to set
     */
    public void setFirstline(String firstline) {
        this.firstline = firstline;
    }

    /**
     * @return the secondline
     */
    public String getSecondline() {
        return secondline;
    }

    /**
     * @param secondline the secondline to set
     */
    public void setSecondline(String secondline) {
        this.secondline = secondline;
    }

    /**
     * @return the towncity
     */
    public String getTowncity() {
        return towncity;
    }

    /**
     * @param towncity the towncity to set
     */
    public void setTowncity(String towncity) {
        this.towncity = towncity;
    }

    /**
     * @return the countystate
     */
    public String getCountystate() {
        return countystate;
    }

    /**
     * @param countystate the countystate to set
     */
    public void setCountystate(String countystate) {
        this.countystate = countystate;
    }

    /**
     * @return the postcode
     */
    public String getPostcode() {
        return postcode;
    }

    /**
     * @param postcode the postcode to set
     */
    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country the country to set
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * @return the notes
     */
    public String getNotes() {
        return notes;
    }

    /**
     * @param notes the notes to set
     */
    public void setNotes(String notes) {
        this.notes = notes;
    }
}
