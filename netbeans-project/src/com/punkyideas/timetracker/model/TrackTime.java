/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.punkyideas.timetracker.model;

import java.io.Serializable;
import java.time.*;
import javax.persistence.*;

@Entity(name = "TIMER")
public class TrackTime implements Serializable {
    
    @Id
    @GeneratedValue
    private int id;
    
    @JoinColumn(name = "task_id")
    @ManyToOne
    private Task task;
    
    private LocalDate startDate;
    private LocalTime startTime;
    private LocalDate finishDate;
    private LocalTime finishTime;
    private Duration duration;
    private String note;
    private double rate;
    
    public TrackTime() {
        this.startDate = LocalDate.now();
        this.startTime = LocalTime.now();
        this.finishTime = LocalTime.now();
        this.finishDate = LocalDate.now();
        this.note = "";
        this.rate = 0.00;
    }
    
    public TrackTime(Task currentTask) {
        this.startDate = LocalDate.now();
        this.startTime = LocalTime.now();
        this.finishTime = LocalTime.now();
        this.finishDate = LocalDate.now();
        this.note = "";
        this.rate = 0.00;
        this.task = currentTask;
    }
    
    public TrackTime(Double nRate, Task currentTask) {
        this.startDate = LocalDate.now();
        this.startTime = LocalTime.now();
        this.finishTime = LocalTime.now();
        this.finishDate = LocalDate.now();
        this.note = "";
        this.rate = nRate;
        this.task = currentTask;
    }
    
    public double getValue() {
        long seconds = duration.getSeconds();
        long absSeconds = Math.abs(seconds);
        String positive = String.format(
                "%d:%02d:%02d",
                absSeconds / 3600,
                (absSeconds % 3600) / 60,
                absSeconds % 60);
        Double lineValue = (rate / 3600) * seconds;
        return lineValue;
    }
    
    public void stopTimer() {
        this.setFinishTime(LocalTime.now());
        this.setFinishDate(LocalDate.now());
        this.setDuration(Duration.between(startTime, finishTime));
    }
    
    public void updateNote(String nNotes) {
        this.setNote(nNotes);
    }

    /**
     * @return the startDate
     */
    public LocalDate getStartDate() {
        return startDate;
    }

    /**
     * @return the startTime
     */
    public LocalTime getStartTime() {
        return startTime;
    }

    /**
     * @return the finishDate
     */
    public LocalDate getFinishDate() {
        return finishDate;
    }

    /**
     * @return the finishTime
     */
    public LocalTime getFinishTime() {
        return finishTime;
    }

    /**
     * @return the duration
     */
    public Duration getDuration() {
        return duration;
    }

    /**
     * @return the note
     */
    public String getNote() {
        return note;
    }

    /**
     * @param note the note to set
     */
    public void setNote(String note) {
        this.note = note;
    }

    /**
     * @return the rate
     */
    public double getRate() {
        return rate;
    }

    /**
     * @param rate the rate to set
     */
    public void setRate(double rate) {
        this.rate = rate;
    }
    
    public void setRate(String nRate) {
        this.rate = Double.parseDouble(nRate);
    }

    /**
     * @return the task
     */
    public Task getTask() {
        return task;
    }

    /**
     * @param task the task to set
     */
    public void setTask(Task task) {
        this.task = task;
    }

    /**
     * @param startDate the startDate to set
     */
    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    /**
     * @param startTime the startTime to set
     */
    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    /**
     * @param finishTime the finishTime to set
     */
    public void setFinishTime(LocalTime finishTime) {
        this.finishTime = finishTime;
    }

    /**
     * @param duration the duration to set
     */
    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    /**
     * @param finishDate the finishDate to set
     */
    public void setFinishDate(LocalDate finishDate) {
        this.finishDate = finishDate;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }
}
