/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.punkyideas.timetracker.model;

import java.io.Serializable;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.time.*;
import java.util.*;
import javax.persistence.*;

@Entity(name = "TASK")
public class Task implements Serializable {

    @Id
    @GeneratedValue
    private int id;

    private String name;
    private String note;
    private int priority;
    private LocalDate dueDate;
    private boolean complete;
    private boolean billed;
    private Double rate;
    private LocalDate dateBilled;
    private LocalDate completionDate;

    private Label label;

    private List tags;

    @JoinColumn(name = "project_id")
    @ManyToOne
    private Project project;

    @JoinColumn(name = "client_id")
    @ManyToOne
    private Client client;

    @OneToMany(targetEntity = TrackTime.class, mappedBy = "task", cascade = CascadeType.ALL)
    private List<TrackTime> timers;

    public Task() {
        this.name = "";
        this.note = "";
        this.priority = 0;
        this.dueDate = LocalDate.now().plusDays(1);
        this.complete = false;
        this.completionDate = null;
        this.timers = new ArrayList<>();
        this.billed = false;
    }

    public Task(String nName, String nNote, int tPriority, String dDate, String nRate, Client tClient) {
        this.name = nName;
        this.note = nName;
        if (tPriority >= -1 && tPriority <= 2) {
            this.priority = tPriority;
        } else {
            this.priority = 0;
        }
        this.rate = Double.parseDouble(nRate);
        this.client = tClient;
        if(dDate == "") {
            this.dueDate = null;
        }
        else {
            this.dueDate = LocalDate.parse(dDate);
        }
        this.complete = false;
        this.completionDate = null;
        this.timers = new ArrayList<>();
        this.billed = false;
    }

    public Duration getTotalTime() {
        Duration totalTimeWorked = Duration.ZERO;

        if (this.timers.size() > 0) {
            for (TrackTime timer : this.timers) {
                totalTimeWorked = totalTimeWorked.plus(timer.getDuration());
            }
        }

        return totalTimeWorked;
    }
    
    public double getTotalValue() {
        Double totalValue = 0.00;

        if (this.timers.size() > 0) {
            for (TrackTime timer : this.timers) {
                totalValue = totalValue + timer.getValue();
            }
        }

        return totalValue;
    }
    
    public String getTotalValueString() {
        DecimalFormat df = new DecimalFormat("#.##");
        df.setRoundingMode(RoundingMode.CEILING);
        return df.format(this.getTotalValue());
    }

    public String getTotalTimeString() {
        long seconds = this.getTotalTime().getSeconds();
        long absSeconds = Math.abs(seconds);
        String positive = String.format(
                "%d:%02d:%02d",
                absSeconds / 3600,
                (absSeconds % 3600) / 60,
                absSeconds % 60);
        return seconds < 0 ? "-" + positive : positive;
    }

    public void addTimer(TrackTime nTimer) {
        this.getTimers().add(nTimer);
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the note
     */
    public String getNote() {
        return note;
    }

    /**
     * @param note the note to set
     */
    public void setNote(String note) {
        this.note = note;
    }

    /**
     * @return the priority
     */
    public int getPriority() {
        return priority;
    }

    /**
     * @param priority the priority to set
     */
    public void setPriority(int priority) {
        this.priority = priority;
    }

    /**
     * @return the dueDate
     */
    public LocalDate getDueDate() {
        return dueDate;
    }

    /**
     * @param dueDate the dueDate to set
     */
    public void setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
    }

    /**
     * @return the complete
     */
    public boolean isComplete() {
        return complete;
    }

    /**
     * @param complete the complete to set
     */
    public void setComplete(boolean complete) {
        this.complete = complete;
    }

    /**
     * @return the billed
     */
    public boolean isBilled() {
        return billed;
    }

    /**
     * @param billed the billed to set
     */
    public void setBilled(boolean billed) {
        this.billed = billed;
    }

    /**
     * @return the completionDate
     */
    public LocalDate getCompletionDate() {
        return completionDate;
    }

    /**
     * @param completionDate the completionDate to set
     */
    public void setCompletionDate(LocalDate completionDate) {
        this.completionDate = completionDate;
    }

    /**
     * @return the label
     */
    public Label getLabel() {
        return label;
    }

    /**
     * @param label the label to set
     */
    public void setLabel(Label label) {
        this.label = label;
    }

    /**
     * @return the tags
     */
    public List getTags() {
        return tags;
    }

    /**
     * @param tags the tags to set
     */
    public void setTags(List tags) {
        this.tags = tags;
    }

    /**
     * @return the project
     */
    public Project getProject() {
        return project;
    }

    /**
     * @param project the project to set
     */
    public void setProject(Project project) {
        this.project = project;
    }

    /**
     * @return the client
     */
    public Client getClient() {
        return client;
    }

    /**
     * @param client the client to set
     */
    public void setClient(Client client) {
        this.client = client;
    }

    /**
     * @return the timers
     */
    public List<TrackTime> getTimers() {
        return timers;
    }

    /**
     * @param timers the timers to set
     */
    public void setTimers(List<TrackTime> timers) {
        this.timers = timers;
    }

    /**
     * @return the rate
     */
    public Double getRate() {
        return rate;
    }

    /**
     * @param rate the rate to set
     */
    public void setRate(Double rate) {
        this.rate = rate;
    }

    /**
     * @return the dateBilled
     */
    public LocalDate getDateBilled() {
        return dateBilled;
    }

    /**
     * @param dateBilled the dateBilled to set
     */
    public void setDateBilled(LocalDate dateBilled) {
        this.dateBilled = dateBilled;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

}
