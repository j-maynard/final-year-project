
package com.punkyideas.timetracker.config;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

public final class AppConfig {
    private boolean enableUni;
    private boolean hideCompleted;
    private String datafile;
    private String dataLocation;
    
    public boolean getUni() {
        return this.enableUni;
    }
    
    public String getDataFile() {
        return this.datafile;
    }
    
    public boolean hideCompleted() {
        return this.hideCompleted;
    }
    
    public void setUni(boolean uniState) {
        this.enableUni = uniState;
    }
    
    public void setDataFile(String newDataFile) {
        this.datafile = newDataFile;
    }
    
    public void setHideCompleted(boolean completed) {
        this.hideCompleted = completed;
    }
   
    public AppConfig() {
        Properties prop = new Properties();
	InputStream input = null;

	try {
            input = new FileInputStream("config.properties");
            // load a properties file
            prop.load(input);
            
            this.datafile = prop.getProperty("datafile");
            this.dataLocation = prop.getProperty("dataLocation");
            this.enableUni = Boolean.parseBoolean(prop.getProperty("enableUni"));
            this.hideCompleted = Boolean.parseBoolean(prop.getProperty("hideCompleted"));
	}
        catch (IOException ex) {
            System.out.println("Recreating new preferences file... Old file missing or corrupt.");
            this.enableUni = false;
            this.datafile = "timetracker.db";
            this.hideCompleted = true;
            this.dataLocation = System.getProperty("user.home");
            savePreferences();
	}
        finally {
            if (input != null) {
                try {
                    input.close();
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
	}
    }
    
    public AppConfig(boolean uniSet) {
        this.enableUni = uniSet;
    }
    
    public AppConfig(String sqlDataFile) {
        this.datafile = sqlDataFile;
    }
    
    public AppConfig(boolean uniSet, String sqlDataFile) {
        this.enableUni = uniSet;
        this.datafile = sqlDataFile;
    }
    
    public void savePreferences() {
        Properties prop = new Properties();
	OutputStream output = null;

	try {
            output = new FileOutputStream("config.properties");
            System.out.println("Saving Preferences File");
            // set the properties value
            prop.setProperty("datafile", this.datafile);
            prop.setProperty("dataLocation", this.dataLocation);
            prop.setProperty("enableUni", Boolean.toString(this.enableUni));
            prop.setProperty("hideCompleted", Boolean.toString(this.hideCompleted));
            

            // save properties to project root folder
            prop.store(output, null);
	}
        catch (IOException io) {
		io.printStackTrace();
	}
        finally {
            if (output != null) {
		try {
                    output.close();
		}
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
	}
    }
    
    public void loadPrefences() {
        Properties prop = new Properties();
	InputStream input = null;

	try {
            input = new FileInputStream("config.properties");
            // load a properties file
            prop.load(input);
            
            this.datafile = prop.getProperty("datafile");
            this.dataLocation = prop.getProperty("dataLocation");
            this.enableUni = Boolean.parseBoolean(prop.getProperty("enableUni"));
            this.hideCompleted = Boolean.parseBoolean(prop.getProperty("hideCompleted"));
	}
        catch (IOException ex) {
            System.out.println("Recreating new preferences file... Old file missing or corrupt.");
            this.enableUni = false;
            this.datafile = "timetracker.db";
            this.hideCompleted = true;
            this.dataLocation = System.getProperty("user.home");
            savePreferences();
	}
        finally {
            if (input != null) {
                try {
                    input.close();
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
	}
    }

    /**
     * @return the dataLocation
     */
    public String getDataLocation() {
        return dataLocation;
    }

    /**
     * @param dataLocation the dataLocation to set
     */
    public void setDataLocation(String dataLocation) {
        System.out.println("Updating Datalocation to: " + dataLocation);
        this.dataLocation = dataLocation;
    }
}