# README #

This is my complete final year project for my BSc (Hons) in Computing Science.  It includes the full source code of the application I developed.  My final year report and my project progress reports.  It is intended to show people the work that I did on this project.

# Project Frozen

This project is now considered frozen and is only here for historical purposes.  I have another private project which is intended to take this work forward.

# Running the project

Should you wish to compile and run the project, download it and open the project in netbeans, build and run the project.  You will need to have Sqlite and Java 8 installed on your machine in order for the project to run.

# Questions

If you have any questions please e-mail me at jamie . maynard @ me .com